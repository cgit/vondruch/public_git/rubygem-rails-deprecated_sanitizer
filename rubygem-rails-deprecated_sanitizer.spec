# Generated from rails-deprecated_sanitizer-1.0.3.gem by gem2rpm -*- rpm-spec -*-
%global gem_name rails-deprecated_sanitizer

Name: rubygem-%{gem_name}
Version: 1.0.3
Release: 1%{?dist}
Summary: Deprecated sanitizer API extracted from Action View
Group: Development/Languages
License: MIT
URL: https://github.com/rails/rails-deprecated_sanitizer
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(activesupport)
BuildRequires: rubygem(actionview)
BuildRequires: rubygem(minitest)
BuildArch: noarch

%description
In Rails 4.2 HTML sanitization has been rewritten using a more secure library.

This gem includes the old behavior shipping with Rails 4.2 and before. It is
strictly provided to ease migration. It will be supported until Rails 5.

%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/




# Run the test suite
%check
pushd .%{gem_instdir}
# Don't use Bundler.
sed -i "/require 'bundler\/setup'/ s/^/#/" test/test_helper.rb

# ActiveSupport::TestCase.test_order is not available in AS 4.1.
sed -i "/\.test_order/ s/^/#/" test/test_helper.rb

ruby -Ilib:test -e 'Dir.glob "./test/**/*_test.rb", &method(:require)'
popd

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/CHANGELOG.md
%doc %{gem_instdir}/README.md
%{gem_instdir}/test

%changelog
* Tue Jan 20 2015 Vít Ondruch <vondruch@redhat.com> - 1.0.3-1
- Initial package
